//A. Ascending Ten
function AscendingTen(input) {
    if (input == undefined) {
        return -1
    }
    var deret = []
    for (let index = 0; index < 10; index++) {
        deret.push(`${input+index}`)
    }
    var result = deret.join(' ')
    return result
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1
console.log("== = == = ==");

//B. Descending Ten
function DescendingTen(input) {
    if (input == undefined) {
        return -1
    }
    var deret = []
    for (let index = 0; index < 10; index++) {
        deret.push(`${input-index}`)
    }
    var result = deret.join(' ')
    return result
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
console.log("== = == = ==");

//C. Conditional Ascending Descending
function ConditionalAscDesc(reference, check) {
    if (check == undefined || reference == undefined) {
        return -1
    }else if (check%2 != 0) {
        return AscendingTen(reference)
    }else return DescendingTen(reference)
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log("== = == = ==");

//D. Papan Ular Tangga
function ularTangga() {
    var result = []
    var counter = 0
    for (let index = 100; index >= 1; index-=20) {
        console.log(DescendingTen(index));
        console.log(AscendingTen(index-19));
    }
}
ularTangga();