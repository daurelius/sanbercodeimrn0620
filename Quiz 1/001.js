//A. Balik String
function balikString(input="") {
    var result = ""
    for (let index = input.length-1; index >= 0; index--) {
        result += input[index]
    }
    return result
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log("== = == = ==");

//B. Palindrome
function palindrome(input) {
    var result = false
    var pointer = input.length-1
    for (let index = 0; index < parseInt(input.length/2); index++) {
        if (input[index] == input[pointer]) {
            result = true
        }else result = false
        pointer--
    }
    return result
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log('== = == = ==');

//C. Bandingkan Angka
function bandingkan(first, second) {
    if (first < 0 || second < 0 || first == second || second == undefined) {
        return -1
    } else if (first > second) {
        return first
    }else return second
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18