//if-else
var nama = "David"
var peran = "Guard"
var introMessage = "Selamat datang di Dunia Warewolf, "
var warningMessage =""
if (nama == "") {
    console.log("Nama harus diisi!");
}else if (nama != "" && peran == "") {
    console.log("Hallo "+nama+", Pilih Peranmu untuk memulai game!");
}else{
    console.log(introMessage + nama);
    if (peran == "Penyihir") {
        console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if (peran == "Guard") {
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }else if (peran == "Werewolf") {
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    }
}
//switch case
var tanggal = 17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 6; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
switch (bulan) {
    case 1:{console.log(tanggal+" Januari "+tahun);break;}
    case 2:{console.log(tanggal+" Februari "+tahun);break;}
    case 3:{console.log(tanggal+" Maret "+tahun);break;}
    case 4:{console.log(tanggal+" April "+tahun);break;}
    case 5:{console.log(tanggal+" Mei "+tahun);break;}
    case 6:{console.log(tanggal+" Juni "+tahun);break;}
    case 7:{console.log(tanggal+" Juli "+tahun);break;}
    case 8:{console.log(tanggal+" Agustus "+tahun);break;}
    case 9:{console.log(tanggal+" September "+tahun);break;}
    case 10:{console.log(tanggal+" Oktober "+tahun);break;}
    case 11:{console.log(tanggal+" November "+tahun);break;}
    case 12:{console.log(tanggal+" Desember "+tahun);break;}
    default:break;
}