import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { SignIn, CreateAccount } from './Screen';

import { StyleSheet, Text, View } from "react-native";

const AuthStack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <AuthStack.Navigator >
          <AuthStack.Screen name='SignIn' component={SignIn}  />
          <AuthStack.Screen name='CreateAccount' component={CreateAccount}  />
        </AuthStack.Navigator>
      </NavigationContainer>
    );
  }
}