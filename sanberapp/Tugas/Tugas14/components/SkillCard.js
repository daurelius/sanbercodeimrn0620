import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class Note extends React.Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //     noteArray:[],
    //     noteText:'',
    //     }
    // }
    render() {
        let skill = this.props.data
        return(
        <View key={this.props.keyval} style={styles.card}>
            <MaterialCommunityIcons style={{padding: 5}} name={skill.iconName} size={100} color="#003366" />
            <View style={styles.skillContainer}>
                <Text style={styles.skillTitle}>{skill.skillName}</Text> 
                <Text style={styles.skillCategory}>{skill.categoryName}</Text>
                <Text style={styles.skillPercentage}>{skill.percentageProgress}</Text>
            </View>
            <TouchableOpacity>
                <AntDesign name="right" size={24} color="black" />
            </TouchableOpacity>
        </View>
        );
    }
};

const styles = StyleSheet.create({
    card:{
        backgroundColor: '#B4E9FF',
        borderRadius: 10,
        marginTop: 15,
        flexDirection: "row",
        alignItems: 'center'
    },
    skillContainer:{
        width: 210,
        paddingVertical: 10,
        paddingRight: 10
    },
    skillTitle:{
        fontSize: 30,
        fontWeight: "bold",
        color: '#003366'
    },
    skillCategory:{
        color: '#56B8FF'
    },
    skillPercentage:{
        color: 'white',
        alignSelf: 'flex-end',
        fontSize: 40,
        fontWeight: "bold"
    }


//   note: {
//     position: 'relative',
//     padding: 20,
//     paddingRight: 100,
//     borderBottomWidth: 2,
//     borderBottomColor: '#ededed',
//   },
//   noteText: {
//     paddingLeft: 20,
//     borderLeftWidth: 10,
//     borderLeftColor: '#e91e63',
//   },
//   noteDelete: {
//     position: 'absolute',
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#2980b9',
//     padding: 10,
//     top: 10,
//     bottom: 10,
//     right: 10
//   },
//   noteDeleteText: {
//     color: 'white',
//   }
});
