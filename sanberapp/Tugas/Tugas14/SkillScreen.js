import React from 'react';
import { Image, View, Text, FlatList, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import SkillCard from './components/SkillCard';
import data from './skillData.json'

// import SkillCard from './SkillCard';

export default class SkillScreen extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            skillArray: data.items
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image source={require('./images/logo(1).png')} style={{height: 60, width: 210}}/>
                </View>
                <View style={styles.userContainer}>
                    <FontAwesome5 name="user-circle" size={30} color="#56B8FF" style={{padding: 10}}/>
                    <View style={styles.greeting}>
                        <Text style={{fontSize: 11}}>Hai,</Text>
                        <Text style={styles.userNameText}>David Aurelius Tandjung</Text>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.titleText}>SKILL</Text>
                    <View style={{ height:3, backgroundColor:'#56B8FF'}}/>
                    <View style={styles.categoryContainer}>
                        <View style={styles.categoryItem}>
                            <Text style={styles.categoryText}>Library/Framework</Text>
                        </View>
                        <View style={styles.categoryItem}>
                            <Text style={styles.categoryText}>Bahasa Pemprograman</Text>
                        </View>
                        <View style={styles.categoryItem}>
                            <Text style={styles.categoryText}>Teknologi</Text>
                        </View>
                    </View>
                    <FlatList
                        style={styles.listContainer}
                        data={this.state.skillArray}
                        renderItem={(data)=> <SkillCard data={data.item}/>}
                        keyExtractor={(item)=> item.id.toString()}
                    />
                </View>
            </View>
        );
    }
};



const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingTop: 17
    },
    userContainer:{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start"
    },
    greeting:{

    },
    userNameText:{
        fontSize: 15,
        color: '#003366'
    },
    body:{
        padding:15,
    },
    titleText:{
        fontSize: 30,
    },
    categoryContainer:{
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around',
        marginVertical: 10
    },
    categoryItem:{
        backgroundColor: '#B4E9FF',
        paddingVertical:6,
        paddingHorizontal: 12,
        borderRadius: 8,
    },
    categoryText:{
        fontSize: 12
    },
    listContainer:{
        marginBottom: 210
    }
});

