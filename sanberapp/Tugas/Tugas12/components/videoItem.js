import React from 'react';
import { Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class App extends React.Component {
	render(){
        let video = this.props.data
		return (
            <View style={styles.container}>
                <Image source={{ uri: video.snippet.thumbnails.medium.url}} style={{height: 200}}/>
                <View style={styles.descContainer}>
                    <Image source={{uri:'https://randomuser.me/api/portraits/men/51.jpg'}} style={{width:50, height:50, borderRadius:25}}/>
                    <View style={styles.videoDetail}>
                        <Text numberOfLines={2} style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle +' · '+nFormatter(video.statistics.viewCount,1)+' · 3 months ago'}</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name='more-vert' size={20} color='#999999'/>
                    </TouchableOpacity>
                </View>
            </View>
		)
	}
};

const styles = StyleSheet.create({
	container:{
        flex: 1,
        padding: 15,
    },
    descContainer:{
        flex: 1,
        flexDirection: 'row',
        paddingTop: 15
    },
    videoDetail:{
        flex: 1,
        paddingHorizontal: 15
    },
    videoTitle:{
        fontSize: 16,
        color: '#3c3c3c'
    },
    videoStats:{
        fontSize: 12,
        color: '#3c3c3c',
        paddingTop: 3
    }
});

function nFormatter(num, digits) {
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
        break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views';
}