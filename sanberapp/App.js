import { StatusBar } from 'expo-status-bar';
// import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import YoutubeUI from './Tugas/Tugas12/App';
import Tugas13 from './Tugas/Tugas13/AboutScreen'
import Tugas14 from './Tugas/Tugas14/SkillScreen'
import Tugas15 from './Tugas/Tugas15/index'
import Quiz3 from './Tugas/Quiz3/index'

export default function App() {
  return (
    <Quiz3/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
