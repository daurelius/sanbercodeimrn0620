//soal no.1
console.log("LOOPING PERTAMA")
var loopCount = 0
while (loopCount < 20) {
    loopCount += 2
    console.log(loopCount + " - I love coding")
}
console.log("LOOPING KEDUA")
while (loopCount >= 2) {
    console.log(loopCount + " - I will become a mobile developer")
    loopCount -= 2
}
//soal no.2
for (let index = 1; index <= 20; index++) {
    if (index % 2 != 0) {
        if (index % 3 == 0) { console.log(index + " - I Love Coding"); }
        else console.log(index + " - Santai");
    } else { console.log(index + " - Berkualitas"); }
}
//soal no.3
for (let index = 0; index < 4; index++) { console.log("########"); };
//soal no.4
var anakTangga = "#######"
for (let index = 1; index <= 7; index++) { console.log(anakTangga.substr(0, index)); }
//soal no.5
for (let index = 1; index <= 8; index++) {
    if (index % 2 == 0) {
        console.log("# # # # ");
    } else console.log(" # # # #");
}