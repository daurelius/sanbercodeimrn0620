//soal no.1 (fungsi arrow)
const golden = () =>{console.log('this is golden!!');}
golden()

console.log("== = == = ==");
//soal no.2 (object)
const newFunction = (firstName, lastName) =>{
    return {
        firstName,
        lastName,
        fullName : () =>{
            console.log(`${firstName} ${lastName}`);
        }
    }
}
newFunction("William", "Imoh").fullName()

console.log("== = == = ==");
//soal no.3 (destructuring)
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const{firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

console.log("== = == = ==");
//soal no.4 (array spreading)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

console.log("== = == = ==");
//soal no.5 (template literals)
const planet = "earth"
const view = "glass"
// var before = '`Lorem ' + view + 'dolor sit amet, ' +
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam`'
var before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`

// Driver Code
console.log(before)