class Score{
    constructor(subject, point = [], email){
        this.subject = subject
        this.point = point
        this.email = email
    }
    average = () =>{
        let sum = 0
        for (let index = 0; index < this.point.length; index++) {
            sum += this.point[index]
        }
        console.log(sum / this.point.length);
        return sum/this.point.length
    }
}

let test = new Score('react', [10], 'david@mail.com')
test.average()

//soal.2 
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
let subject = "quiz-1"
const viewScores = ( data =[[]], subject = "") =>{
    let result = []
    for (let i = 1; i < data.length; i++) {
        let point = []
        for (let j = 0; j < 3; j++) {
            point.push(data[i][1+j])
        }
        result.push(new Score(subject, point, data[i][0]))
    }
    console.log(result);
    
}
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//soal.3
const recapScore = (data) =>{
    for (let i = 1; i < data.length; i++) {
        console.log(`${i}. Email: ${data[i][0]}`);
        let sum = 0
        for (let j = 1; j < data[i].length; j++) {
            sum += data[i][j]
        }
        let average = sum / 3
        console.log(`Rata-rata: ${average}`);
        if (average>90) {
            console.log('Predikat: Honour');
        }else if(average>80){
            console.log('Predikat: Graduate');
        }else{
            console.log('Predikat: Participant');
        }
    }
}
recapScore(data)
// 1. Email: abduh @mail.com
// Rata - rata: 85.7
// Predikat: graduate

// 2. Email: khairun @mail.com
// Rata - rata: 89.3
// Predikat: graduate

// 3. Email: bondra @mail.com
// Rata - rata: 74.3
// Predikat: participant

// 4. Email: regi @mail.com
// Rata - rata: 91
// Predikat: honour